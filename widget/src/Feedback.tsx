import React from 'react';
import { LoanScoreResult } from './model/LoanScoreRequest';

interface IProps {
    result:LoanScoreResult;
 }

const Feedback: React.FC<IProps> = (props:IProps)=>{
    return (
        <div className="submitted-info flex-center">
            <p>Congratulations you are now signed up to loanScore by uno</p>
            <p>Your loanScore is <span className="loan-score">{props.result.loanScore}</span></p>            
            <a href={props.result.loanScoreWebUrl} target="_blank" rel="noopener noreferrer"><button className="button button-uno done-button">View your loanScore details</button></a>            
        </div>
    )
}

export default Feedback;