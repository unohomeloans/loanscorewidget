import axios from "axios";
import React, { useState, useRef, useEffect } from "react";
import { LoanPurpose } from "./Constants";
import { LoanScoreRequest, LoanScoreResult } from "./model/LoanScoreRequest";
// eslint-disable-next-line
import { getDummyEmail } from "./Util";
import { to } from "./SharedHelpers";
import { InputTypeAheadV2 } from "@unowildcard/components";
import { InputTextV2 } from "@unowildcard/components";
import useAsyncEffect from "use-async-effect";
import { LenderItem } from "./model/LendersList";
import LoanScoreCta from "./LoanScoreCta";
import LoanScoreResultView from "./LoanScoreResultView";

import { Flex, Box } from "reflexbox";

interface IProps {
  referrerLabel: string;
  baseUrl: string;
  onSubmit: (request: LoanScoreRequest) => void;
  submitting: boolean;
  errorMessage: string | undefined;
  loanScoreResult?: LoanScoreResult;
}
interface IFormErrors {
  [x: string]: string | undefined;
}

interface InputTypeAheadV2Item {
  display: string;
  value: string;
}

const LoanScoreForm: React.FC<IProps> = (props: IProps) => {
  const [isValid, setIsValidState] = useState(false);
  const [lenders, setLenders] = useState<LenderItem[]>([]);
  const [lendersFiltered, setLendersFiltered] = useState<
    InputTypeAheadV2Item[]
  >([]);
  const formErrors = useRef({} as IFormErrors);

  const loanAmount = useRef('');
  const currentRate = useRef('');
  const remainingLoanTerm = useRef('');
  const grossSalary = useRef('');
  const currentLender = useRef('');
  const email = useRef('');
  const name = useRef('');
  const phone = useRef('');
  const address = useRef('');

  // const loanAmount = useRef("338888");
  // const currentRate = useRef("4.2");
  // const remainingLoanTerm = useRef("2000");
  // const grossSalary = useRef("100000");
  // const currentLender = useRef("BankWest");
  // const email = useRef(getDummyEmail("loanscorewidget"));
  // const name = useRef("John Doe");
  // const phone = useRef("0491570006");
  // const address = useRef("20 Gubbuteh Road, Little Bay, NSW 2036");

  const setIsValid = (name: string, error?: string) => {
    formErrors.current[name] = error;
    let message = validateSubmit().message;
    //console.log('validateSubmit', message)
    //console.log('currentLender', currentLender)
    setIsValidState(!message);
  };

  const [acceptTerms, setAcceptTerms] = useState(true);
  const [purpose, setLoanPurpose] = useState(LoanPurpose.OwnerOccupied);
  const [errorMessage, setErrorMessage] = useState(props.errorMessage);

  useAsyncEffect(async (isMounted) => {
    const [err, axiosResult] = await to(axios.get(`${props.baseUrl}/lenders?referrerLabel=${props.referrerLabel}`));
    if (!err && isMounted()) {
      let lenders = axiosResult.data;
      let items = lenders.map((x: LenderItem) => ({
        display: x.name,
        value: x.code,
      }));
      setLenders(lenders);
      setLendersFiltered(items);
    }
  }, []);

  useEffect(() => {
    setErrorMessage(props.errorMessage);
  }, [props.errorMessage]);

  const validateSubmit = (): { success?: boolean; message?: string } => {
    if (!loanAmount.current) {
      return { message: "Loan Amount is Required" };
    }

    if (!currentRate.current) {
      return { message: "Current Rate is Required" };
    }
    if (!remainingLoanTerm.current) {
      return { message: "Monthly Repayment is Required" };
    }

    if (!currentLender.current) {
      return { message: "Current Lender is Required" };
    }

    if (!grossSalary.current) {
      return { message: "Gross Salary is Required" };
    }

    // if(!estimatedValue.current){
    //     return { message:'Estimated Value is Required' }
    // }

    const { firstName, lastName } = getName(name.current);
    if (!firstName || !lastName) {
      return { message: "Please provide your Full Name e.g. John Smith" };
    }

    if (!email.current) {
      return { message: "Email is Required" };
    }

    if (!acceptTerms) {
      return { message: "You must agree to the terms & conditions" };
    }

    for (let error in formErrors.current) {
      if (formErrors.current[error]) {
        return { message: formErrors.current[error] };
      }
    }

    return { success: true, message: "" };
  };

  const getName = (text: string): { firstName: string; lastName: string } => {
    const tokens = text.split(" ");
    let firstName = "";
    let lastName = "";
    if (tokens.length > 1) {
      firstName = tokens[0];
      lastName = tokens.slice(1).join(" ");
    }

    return { firstName, lastName };
  };

  let onClick = () => {
    const validateResult = validateSubmit();
    if (!validateResult.success) {
      setErrorMessage(validateResult.message);
      return;
    }
    setErrorMessage('');
    const { firstName, lastName } = getName(name.current);
    let emailAddress = email.current;
    let lenderCode = lenders.find((x) => x.name === currentLender.current)
      ?.code!;
    let request: LoanScoreRequest = {
      referrerLabel: props.referrerLabel,
      newUser: {
        email: emailAddress,
        firstName: firstName,
        lastName: lastName,
        mobileNumber: phone.current,
        termsOfUseConsent: true,
      },
      finances: {
        householdIncome: {
          grossAmount: +grossSalary.current,
          frequency: "YEARLY",
        },
      },
      homeLoan: {
        loanAccounts: [
          {
            remainingAmount: +loanAmount.current,
            // "remainingLoanTerm": +remainingLoanTerm.current,
            // "remainingLoanTermDurationUnit": "YEARLY",
            repaymentAmount: +remainingLoanTerm.current,
            repaymentFrequency: "MONTHLY",
            repaymentType: "PRINCIPAL_AND_INTEREST",
            interestRate: +currentRate.current,
            lenderCode: lenderCode,
          },
        ],
        realEstates: [],
      },
      property: {
        purpose: purpose.toString(),
        //"estimatedValues": [ { "basis" : "USER", "value": +estimatedValue.current }],
        estimatedValues: [],
        address: {
          fullAddress: address.current,
        },
      },
    };
    props.onSubmit(request);
  };

  return (
    <form>
      <Flex flexWrap="wrap">
        <Box width={[1, 1, 1, 2/3]} p={3}>
          <Flex flexWrap="wrap" className="card">
            <Box id="" width={[1, 1, 1 / 2]} p={3}>
              <InputTextV2
                labelText="Loan Amount"
                idSelector="uno-loanScore-widget-loanAmount"
                additionalClasses="input-100"
                inputHintForError="Please enter a value between 10,000 and 10,000,000. e.g. 400,000"
                placeHolderText="e.g. 400,000"
                min={10000}
                max={10000000}
                numberOnly
                showCommaSeparator
                showDollarSign
                value={loanAmount.current}
                onChange={(value?: string, error?: string) => {
                  loanAmount.current = value!;
                  setIsValid("loanAmount", error);
                }}
              />

              <div className="inlined-fields input-100 grouped">
                <InputTextV2
                  labelText="Current Rate"
                  idSelector="uno-loanScore-widget-currentRate"
                  placeHolderText="e.g. 4.15"
                  min={0.1}
                  max={15}
                  inputHintForError="Please enter a valid value. E.g. 4.21"
                  forceMaskValidInput
                  forceMask={[/\d/, ".", /\d/, /\d/]}
                  floatPointPrecision={2}
                  showPercentSign
                  value={currentRate.current}
                  onChange={(value?: string, error?: string) => {
                    currentRate.current = value!;
                    setIsValid("currentRate", error);
                  }}
                />

                <InputTextV2
                  labelText="Monthly Repayment"
                  idSelector="uno-loanScore-widget-remainingLoanTerm"
                  additionalClasses="remaining-term"
                  placeHolderText=""
                  inputHintForError="Please enter a value between 100 and 100,000"
                  min={100}
                  max={100000}
                  numberOnly
                  value={remainingLoanTerm.current}
                  onChange={(value?: string, error?: string) => {
                    remainingLoanTerm.current = value!;
                    setIsValid("remainingLoanTerm", error);
                  }}
                />
              </div>

              <InputTextV2
                labelText="Gross Salary"
                idSelector="uno-loanScore-widget-grossSalary"
                additionalClasses="input-100"
                inputHintForError="Please enter a value between 1 and 10,000,000. e.g. 400,000"
                placeHolderText="e.g. 45,000"
                min={1}
                max={10000000}
                numberOnly
                showCommaSeparator
                showDollarSign
                value={grossSalary.current}
                onChange={(value?: string, error?: string) => {
                  grossSalary.current = value!;
                  setIsValid("grossSalary", error);
                }}
              />

              <div>
                <label style={{ marginBottom: "10px", fontFamily: "Avenir-Medium, sans-serif" }}>Current Lender</label>
                <InputTypeAheadV2
                  idSelector={"uno-loanScore-widget-currentLender"}
                  textLabel="Current Lender"
                  additionalClasses="input-100"
                  showClearIcon
                  showBackButton
                  highlightLastRow
                  placeHolder={"e.g. Westpac"}
                  listItems={lendersFiltered}
                  value={currentLender.current}
                  onChange={(
                    e?: { target: { value: string } },
                    error?: string
                  ) => {
                    if (e) {
                      let val = e!.target.value.toLowerCase();
                      let arr = lenders.filter(
                        (x) =>
                          x.code.toLocaleLowerCase().startsWith(val) ||
                          x.name.toLocaleLowerCase().startsWith(val)
                      );
                      let otherItem = lenders.find(
                        (x) => x.name.toLowerCase() === "other"
                      );
                      if (otherItem) {
                        arr.push(otherItem);
                      }

                      setLendersFiltered(
                        arr.map((x) => ({ display: x.name, value: x.code }))
                      );
                    }
                  }}
                  onSelect={(item?: InputTypeAheadV2Item, error?: string) => {
                    currentLender.current = item!.display;
                    setIsValid("currentLender", error);
                  }}
                />
              </div>

              <InputTextV2
                labelText="Property Address"
                idSelector="uno-loanScore-widget-address"
                inputHintForError="Please enter your address"
                additionalClasses="input-100"
                placeHolderText="e.g. 3 Smith Street"
                value={address.current}
                onChange={(value?: string, error?: string) => {
                  address.current = value!;
                  //setIsValid('name', error);
                }}
              />
            </Box>
            <Box id="" width={[1, 1, 1 / 2]} p={3}>
              <div className="input-100">
                <label style={{ marginBottom: "10px", fontFamily: "Avenir-Medium, sans-serif" }}>Do you live in the property?</label>

                <div className="loan-purpose-button-container">
                  <button
                    name={LoanPurpose.OwnerOccupied}
                    type="button"
                    className={
                      "button yes-no " +
                      (purpose === LoanPurpose.OwnerOccupied
                        ? "yes-no-selected"
                        : "")
                    }
                    onClick={(e: any) => setLoanPurpose(e.target.name)}
                  >
                    Yes
                  </button>
                  <button
                    name={LoanPurpose.Investment}
                    type="button"
                    className={
                      "button yes-no " +
                      (purpose === LoanPurpose.Investment
                        ? "yes-no-selected"
                        : "")
                    }
                    onClick={(e: any) => setLoanPurpose(e.target.name)}
                  >
                    No, it's an Investment
                  </button>
                </div>
              </div>

              {/* <InputTextV2
                            labelText="Estimated Value"
                            idSelector="uno-loanScore-widget-estimatedValue"
                            additionalClasses="input-100"
                            inputHintForError="Please enter a value between 10,000 and 10,000,000. e.g. 400,000"
                            placeHolderText="e.g. 400,000"
                            min={10000}
                            max={10000000}
                            numberOnly
                            showCommaSeparator
                            showDollarSign
                            value={estimatedValue.current}
                            onChange={(value?: string, error?:string)=>{
                                estimatedValue.current = value!
                                setIsValid('estimatedValue', error);
                            }}
                            /> */}

              <InputTextV2
                labelText="Your Name"
                idSelector="uno-loanScore-widget-name"
                inputHintForError="Please enter your first and last name"
                additionalClasses="input-100"
                placeHolderText="e.g. John Smith"
                value={name.current}
                onChange={(value?: string, error?: string) => {
                  name.current = value!;
                  setIsValid("name", error);
                }}
                customValidator={(value: string) => {
                  const { firstName, lastName } = getName(name.current);
                  return { failsValidation: !firstName || !lastName };
                }}
              />

              <InputTextV2
                labelText="Mobile Number"
                idSelector="uno-loanScore-widget-phone"
                inputHintForError="Please enter your mobile (optional)"
                additionalClasses="input-100"
                placeHolderText="mobile (optional)"
                value={phone.current}
                onChange={(value?: string, error?: string) => {
                  phone.current = value!;
                  //setIsValid('name', error);
                }}
              />

              <InputTextV2
                labelText="Email"
                idSelector="uno-loanScore-widget-email"
                inputHintForError="Please enter a valid email. E.g. john.doe@mail.com"
                additionalClasses="input-100"
                emailOnly
                autoComplete={"email"}
                value={email.current}
                onChange={(value?: string, error?: string) => {
                  email.current = value!;
                  setIsValid("email", error);
                }}
              />
            </Box>
          </Flex>
        </Box>

        <Box width={[1, 1, 1, 1/3]} p={3}>
          <div className="card card-fullheight">
            <div className="last-column">
              <div className="uno-logo"></div>
            </div>

            {props.loanScoreResult &&
              !props.loanScoreResult?.errors?.length && (
                <LoanScoreResultView loanScoreResult={props.loanScoreResult} />
              )}

            {(!props.loanScoreResult || errorMessage) && (
              <LoanScoreCta
                onClick={onClick}
                setAcceptTerms={setAcceptTerms}
                acceptTerms={acceptTerms}
                isValid={isValid}
                submitting={props.submitting}
                errorMessage={errorMessage}
                referrerLabel={props.referrerLabel}
              />
            )}
          </div>
        </Box>
      </Flex>
    </form>
  );
};

export default LoanScoreForm;
