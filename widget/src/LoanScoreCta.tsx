import React, {  ChangeEvent } from 'react';
import styled from 'styled-components';

interface IProps {
    onClick:()=>void
    setAcceptTerms:(v:boolean)=>void
    acceptTerms:boolean
    isValid:boolean
    submitting:boolean
    errorMessage:string|undefined
    referrerLabel:string
 }

 const WCLoanScoreWrapperArcMarker = styled.div`
	position: absolute;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	display: block;
`;

const WCLoanScoreWrapperArcMarkerItem = styled.div`
	width: 32px;
	height: 32px;
	transform-origin: center;
	border-style: solid;
	border-width: 0 28px 50px 28px;
	border-color: transparent transparent #fff transparent;
    transform: rotate(calc(360deg / 4)) translateY(-3.2em);

	@media (max-width: 320px) {
        transform: rotate(calc(360deg / 4)) translateY(-4.3em);
	}
`;


const LoanScoreCta: React.FC<IProps> = (props:IProps) => {

    const markerClassName = `c-loan-score__wrapper--arc__marker__item c-loan-score__wrapper--arc__marker__item-45deg`;

    return (
        <>
            <div className="social-proof score">
                <div className="loanScore-wrapper">
                    <div className="loanScore-arc">
                        <div className="loanScore-number">
                            <div className="loanScore-number--score">???</div>
                            <div className="loanScore-number--prefix">/100</div>
                        </div>
                        <WCLoanScoreWrapperArcMarker
							id="arc"
							className="c-loan-score__wrapper--arc__marker"
						>
                            <WCLoanScoreWrapperArcMarkerItem
								className={markerClassName}
							/>
                        </WCLoanScoreWrapperArcMarker>
                    </div>
                </div>
            </div>
            <div className="flex-center last-column savings">
                <p style={{fontSize:'48px', fontWeight:'bold'}}>????</p>
            </div>

            <div className="flex-center last-column">
                <button
                    className="button button-uno flex-center input-100"
                    type="button"
                    onClick={props.onClick}
                    disabled={!props.isValid}
                    title={props.isValid ? '' : 'Complete fields to enable submission'}
                >
                    <span>Get My loanScore</span> <span
                        className={"submitting-animation " + (props.submitting ? 'loader' : '')}></span></button>


            </div>

            {props.errorMessage && !props.submitting && <div className="last-column flex-center error-summary">{props.errorMessage}</div>}

            <div className="flex-horizontal last-column">

                <label className="uno-terms-and-conditions estimated">
                    <input type="checkbox"
                        name="terms"
                        onChange={(e: ChangeEvent<HTMLInputElement>) => props.setAcceptTerms(!props.acceptTerms)}
                        checked={props.acceptTerms} />
                        By continuing, I agree to uno's Website <a href="https://unohomeloans.com.au/terms-and-conditions/" target="_blank" rel="noopener noreferrer">Terms & Conditions</a>,
                        <a href="https://unohomeloans.com.au/active-home-loan-management-terms" target="_blank" rel="noopener noreferrer"> Active Home Loan Management Terms</a> and to us using your information as per our
                        <a href="https://unohomeloans.com.au/privacy-policy/" target="_blank" rel="noopener noreferrer"> Privacy Policy</a> (incl. collection statement) and
                        giving you documents electronically under our <a href="https://unohomeloans.com.au/privacy-policy/#econsent" rel="noopener noreferrer">e-consent. </a>
                    {props.referrerLabel} may receive a referral fee for introducing you to uno Home Loans.
                              </label>
            </div>
        </>
    );
}


export default LoanScoreCta;
