import React from 'react';
import styled, { css } from 'styled-components';

import { LoanScoreResult } from './model/LoanScoreRequest';

interface IProps {
    loanScoreResult:LoanScoreResult | any
 }

 const rotate = css<IProps>`
	transform: rotate(${({ loanScoreResult }) => `${19 + loanScoreResult * 3.2}deg` || 0})
		translateY(-3.2em);

	@media (max-width: 320px) {
		transform: rotate(${({ loanScoreResult }) => `${19 + loanScoreResult * 3.2}deg` || 0})
			translateY(-4.3em);
	}
`;

const WCLoanScoreWrapperArcMarker = styled.div`
	position: absolute;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	display: block;
`;

const WCLoanScoreWrapperArcMarkerItem = styled.div<IProps>`
	${rotate}
	width: 32px;
	height: 32px;
	transform-origin: center;
	border-style: solid;
	border-width: 0 28px 50px 28px;
	border-color: transparent transparent #fff transparent;
`;


const LoanScoreResultView: React.FC<IProps> = (props:IProps) => {

    const markerClassName = `c-loan-score__wrapper--arc__marker__item c-loan-score__wrapper--arc__marker__item-${props.loanScoreResult.loanScore}deg`;

    return (
        <>
            <div className="flex-center last-column with-score">
                <div className="loanScore-wrapper">
                    <div className="loanScore-arc">
                        <div className="loanScore-number">
                            <div className="loanScore-number--score">{props.loanScoreResult.loanScore}</div>
                            <div className="loanScore-number--prefix">/100</div>
                        </div>
                        <WCLoanScoreWrapperArcMarker
							id="arc"
							className="c-loan-score__wrapper--arc__marker"
						>
                            <WCLoanScoreWrapperArcMarkerItem
								className={markerClassName}
								{...props}
							/>
                        </WCLoanScoreWrapperArcMarker>
                    </div>
                </div>
            </div>
            <div className="flex-center last-column savings">
            <p style={{fontSize:'48px', fontWeight:'bold'}}>
                ${props.loanScoreResult.savings?.value.toLocaleString()}</p>
            </div>
            <div className="last-column estimated" style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
            <p>Estimated Net Savings </p>
            <p>over {props.loanScoreResult.savings?.duration} {props.loanScoreResult.savings?.unit.toLowerCase()+'s'}</p>

            </div>


        </>
    );
}


export default LoanScoreResultView;
