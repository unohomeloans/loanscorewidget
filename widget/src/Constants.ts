export enum LoanPurpose {
    OwnerOccupied='OWNER_OCCUPIED',
    Investment='INVESTMENT'
}