export interface SelectOptionItem {
    value:string;
    display:string;
    code?:string;
    aliases?:string[];
}