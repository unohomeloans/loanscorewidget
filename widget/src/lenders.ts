import { SelectOptionItem } from "./SelectOptionItem";

const lenders:SelectOptionItem[] = [
	{
	   "value":"AdelaideBank",
	   "display":"Adelaide Bank",
	   "code":"ADB",
	   "aliases":[
		  "adb"
	   ]
	},
	{
	   "value":"AMP",
	   "display":"AMP",
	   "code":"AMP"
	},
	{
	   "value":"AMS",
	   "display":"AMS",
	   "code":"Other"
	},
	{
	   "value":"ANZ",
	   "display":"ANZ Bank",
	   "code":"ANZ"
	},
	{
	   "value":"AXA",
	   "display":"AXA",
	   "code":"Other"
	},
	{
	   "value":"AustralianMilitaryBank",
	   "display":"Australian Military Bank",
	   "code":"Other"
	},
	{
	   "value":"AuswideBank",
	   "display":"Auswide Bank",
	   "code":"Other"
	},
	{
	   "value":"Aussie",
	   "display":"Aussie Home Loans",
	   "code":"Other"
	},
	{
	   "value":"AMOGroup",
	   "display":"AMO Group",
	   "code":"Other"
	},
	{
	   "value":"ArabBankAustralia",
	   "display":"Arab Bank Australia",
	   "code":"Other"
	},
	{
	   "value":"BankOfMelbourne",
	   "display":"Bank Of Melbourne",
	   "code":"BOM",
	   "aliases":[
		  "bom"
	   ]
	},
	{
	   "value":"BankOfQueensland",
	   "display":"Bank Of Queensland",
	   "code":"Other"
	},
	{
	   "value":"BankOfSouthAustralia",
	   "display":"Bank Of South Australia",
	   "code":"BSA",
	   "aliases":[
		  "bank sa"
	   ]
	},
	{
	   "value":"BankWest",
	   "display":"BankWest",
	   "code":"BWT"
	},
	{
	   "value":"BMM",
	   "display":"Better Mortgage Management",
	   "code":"BMM"
	},
	{
	   "value":"Bluestone",
	   "display":"Bluestone Mortgages",
	   "code":"BLU"
	},
	{
	   "value":"BeyondBank",
	   "display":"Beyond Bank",
	   "code":"Other"
	},
	{
	   "value":"BankofSydney",
	   "display":"Bank of Sydney",
	   "code":"BOS",
	   "aliases":[
		  "bos"
	   ]
	},
	{
	   "value":"BankVic",
	   "display":"BankVic",
	   "code":"Other"
	},
	{
	   "value":"bcu",
	   "display":"bcu",
	   "code":"Other"
	},
	{
	   "value":"BankAustralia",
	   "display":"Bank Australia",
	   "code":"Other"
	},
	{
	   "value":"BnEPersonalBanking",
	   "display":"B&E Personal Banking",
	   "code":"Other"
	},
	{
	   "value":"BendigoBank",
	   "display":"Bendigo Bank",
	   "code":"Other"
	},
	{
	   "value":"CBA",
	   "display":"Commonwealth Bank",
	   "code":"CBA",
	   "aliases":[
		  "cba",
		  "commbank"
	   ]
	},
	{
	   "value":"ChallengeBank",
	   "display":"Challenge Bank",
	   "code":"Other"
	},
	{
	   "value":"Citibank",
	   "display":"Citibank",
	   "code":"CTI"
	},
	{
	   "value":"CommunityFirstCU",
	   "display":"Community First Credit Union",
	   "code":"Other"
	},
	{
	   "value":"CUA",
	   "display":"CUA",
	   "code":"CUA"
	},
	{
	   "value":"ClickLoans",
	   "display":"Click Loans",
	   "code":"Other"
	},
	{
	   "value":"CatalystMoney",
	   "display":"Catalyst Money",
	   "code":"Other"
	},
	{
	   "value":"CoastlineCreditUnion",
	   "display":"Coastline Credit Union",
	   "code":"Other"
	},
	{
	   "value":"DefenceBank",
	   "display":"Defence Bank",
	   "code":"Other"
	},
	{
	   "value":"DelphiBank",
	   "display":"Delphi Bank",
	   "code":"Other"
	},
	{
	   "value":"EasyStreetFinServices",
	   "display":"Easy Street Financial Services",
	   "code":"Other"
	},
	{
	   "value":"ECUAustralia",
	   "display":"ECU Australia",
	   "code":"Other"
	},
	{
	   "value":"Firstmac",
	   "display":"Firstmac Ltd",
	   "code":"Other"
	},
	{
	   "value":"FCCSCreditUnion",
	   "display":"FCCS Credit Union",
	   "code":"Other"
	},
	{
	   "value":"FirstOptionCreditUnion",
	   "display":"First Option Credit Union",
	   "code":"Other"
	},
	{
	   "value":"FreedomLend",
	   "display":"Freedom Lend",
	   "code":"Other"
	},
	{
	   "value":"GatewayCreditUnion",
	   "display":"Gateway Credit Union",
	   "code":"Other"
	},
	{
	   "value":"GnCMutualBank",
	   "display":"G&C Mutual Bank",
	   "code":"Other"
	},
	{
	   "value":"Heritage",
	   "display":"Heritage Bank",
	   "code":"Other"
	},
	{
	   "value":"HomeloansLtd",
	   "display":"Home loans Ltd",
	   "code":"HLL"
	},
	{
	   "value":"Homeside",
	   "display":"Homeside Lending",
	   "code":"Other"
	},
	{
	   "value":"HSBC",
	   "display":"HSBC Bank",
	   "code":"Other"
	},
	{
	   "value":"HolidayCoastCU",
	   "display":"Holiday Coast Credit Union",
	   "code":"Other"
	},
	{
	   "value":"HunterUnited",
	   "display":"Hunter United",
	   "code":"Other"
	},
	{
	   "value":"HumeBank",
	   "display":"Hume Bank",
	   "code":"Other"
	},
	{
	   "value":"HorizonCreditUnion",
	   "display":"Horizon Credit Union",
	   "code":"Other"
	},
	{
	   "value":"HomestarFinance",
	   "display":"Homestar Finance",
	   "code":"Other"
	},
	{
	   "value":"ING",
	   "display":"ING Bank",
	   "code":"ING"
	},
	{
	   "value":"iMortgage",
	   "display":"iMortgage",
	   "code":"Other"
	},
	{
	   "value":"IMB",
	   "display":"IMB Bank",
	   "code":"Other"
	},
	{
	   "value":"IllawarraCUNSW",
	   "display":"Illawarra Credit Union",
	   "code":"Other"
	},
	{
	   "value":"IntechCreditUnion",
	   "display":"Intech Credit Union",
	   "code":"Other"
	},
	{
	   "value":"LaTrobe",
	   "display":"La Trobe Financial",
	   "code":"LTB"
	},
	{
	   "value":"Liberty",
	   "display":"Liberty Financial",
	   "code":"LIB"
	},
	{
	   "value":"loanscomau",
	   "display":"loans.com.au",
	   "code":"Other"
	},
	{
	   "value":"Macquarie",
	   "display":"Macquarie Bank",
	   "code":"MQB"
	},
	{
	   "value":"MEBank",
	   "display":"ME Bank",
	   "code":"MEB"
	},
	{
	   "value":"MortgageHouse",
	   "display":"Mortgage House",
	   "code":"MHO"
	},
	{
	   "value":"MAS",
	   "display":"Mortgage Asset Services",
	   "code":"MAS"
	},
	{
	   "value":"MyState",
	   "display":"MyState Bank",
	   "code":"Other"
	},
	{
	   "value":"MyCreditUnion",
	   "display":"My Credit Union",
	   "code":"Other"
	},
	{
	   "value":"MOVEPeopleDrivenBanking",
	   "display":"Move Bank",
	   "code":"Other"
	},
	{
	   "value":"MacquarieCreditUnion",
	   "display":"Macquarie Credit Union",
	   "code":"Other"
	},
	{
	   "value":"NAB",
	   "display":"NAB",
	   "code":"NAB",
	   "aliases":[
		  "national australia bank",
		  "nab"
	   ]
	},
	{
	   "value":"NewcastlePermanent",
	   "display":"Newcastle Permanent",
	   "code":"Other"
	},
	{
	   "value":"Pepper",
	   "display":"Pepper Money",
	   "code":"PPR"
	},
	{
	   "value":"PnNBank",
	   "display":"P&N Bank",
	   "code":"Other"
	},
	{
	   "value":"PoliceBank",
	   "display":"Police Bank",
	   "code":"Other"
	},
	{
	   "value":"PeoplesChoiceCreditUnion",
	   "display":"People’s Choice Credit Union",
	   "code":"Other"
	},
	{
	   "value":"PacificMortgageGroup",
	   "display":"Pacific Mortgage Group",
	   "code":"Other"
	},
	{
	   "value":"QBE",
	   "display":"QBE LMI",
	   "code":"QBE"
	},
	{
	   "value":"QudosBank",
	   "display":"Qudos Bank",
	   "code":"QML"
	},
	{
	   "value":"QueenslandersCU",
	   "display":"Queenslanders Credit Union",
	   "code":"Other"
	},
	{
	   "value":"QBANK",
	   "display":"QBank",
	   "code":"Other"
	},
	{
	   "value":"QTMutualBank",
	   "display":"QT Mutual Bank",
	   "code":"Other"
	},
	{
	   "value":"RAMS",
	   "display":"RAMS",
	   "code":"Other"
	},
	{
	   "value":"RedZed",
	   "display":"RedZed Lending Solutions",
	   "code":"RZD"
	},
	{
	   "value":"RESIMortgageCorp",
	   "display":"RESI Mortgage Corporation",
	   "code":"Other"
	},
	{
	   "value":"RegionalAustraliaBank",
	   "display":"Regional Australia Bank",
	   "code":"Other"
	},
	{
	   "value":"ReduceHomeLoans",
	   "display":"Reduce Home Loans",
	   "code":"Other"
	},
	{
	   "value":"StGeorge",
	   "display":"St George Bank",
	   "code":"STG"
	},
	{
	   "value":"Suncorp",
	   "display":"Suncorp Bank",
	   "code":"SCP"
	},
	{
	   "value":"SuncorpMetway",
	   "display":"Suncorp Metway",
	   "code":"Other"
	},
	{
	   "value":"SCU",
	   "display":"SCU",
	   "code":"Other"
	},
	{
	   "value":"StateCustodians",
	   "display":"State Custodians",
	   "code":"Other"
	},
	{
	   "value":"SummerlandCreditUnion",
	   "display":"Summerland Credit Union",
	   "code":"Other"
	},
	{
	   "value":"SouthernCrossCreditUnion",
	   "display":"Southern Cross Credit Union",
	   "code":"Other"
	},
	{
	   "value":"SERVICEONEAllianceBank",
	   "display":"Service One Alliance Bank",
	   "code":"Other"
	},
	{
	   "value":"SelectEncompassCreditUnion",
	   "display":"Select Encompass Credit Union",
	   "code":"Other"
	},
	{
	   "value":"TeachersMutualBank",
	   "display":"Teachers Mutual Bank",
	   "code":"TMB"
	},
	{
	   "value":"TheRock",
	   "display":"The Rock",
	   "code":"Other"
	},
	{
	   "value":"TransportMutualCreditUnion",
	   "display":"Transport Mutual Credit Union",
	   "code":"Other"
	},
	{
	   "value":"TheMutual",
	   "display":"The Mutual",
	   "code":"Other"
	},
	{
	   "value":"TheMac",
	   "display":"The Mac",
	   "code":"Other"
	},
	{
	   "value":"TheCapricornian",
	   "display":"The Capricornian",
	   "code":"Other"
	},
	{
	   "value":"UniBank",
	   "display":"UniBank",
	   "code":"Other"
	},
	{
	   "value":"UBank",
	   "display":"UBank",
	   "code":"Other"
	},
	{
	   "value":"VictoriaTeachersMutualBank",
	   "display":"Victoria Teachers Mutual Bank",
	   "code":"Other"
	},
	{
	   "value":"VirginMoney",
	   "display":"Virgin Money",
	   "code":"Other"
	},
	{
	   "value":"Westpac",
	   "display":"Westpac Bank",
	   "code":"WBC",
	   "aliases":[
		  "WBC"
	   ]
	},
	{
	   "value":"YellowBrickRoad",
	   "display":"Yellow Brick Road",
	   "code":"Other"
	},
	{
	   "value":"FirefightersMutual",
	   "display":"Fire Fighters Mutual",
	   "code":"FMB"
	},
	{
	   "value":"HealthProfessionalBank",
	   "display":"Health Professional Bank",
	   "code":"HPB"
	},
	{
	   "value":"Other",
	   "display":"Other",
	   "code":"Other"
	}
 ]
 export default lenders;