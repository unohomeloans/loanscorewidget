import React, { useState } from 'react';
import axios from 'axios';
import { Container } from './Container';
import LoanScoreForm from './LoanScoreForm';
import { LoanScoreRequest, LoanScoreResult } from './model/LoanScoreRequest';
import { to } from './SharedHelpers';


interface IProps {
   referrerLabel: string;
   baseUrl: string;
}


const App: React.FC<IProps> = (props:IProps) => {

   //const [submitted, setSubmitted] = useState(false);
   const [submitting, setSubmitting] = useState(false);
   const [errorMessage, setErrorMessage] = useState('');
   // let loanScoreResult:LoanScoreResult = {
   //    loanScore: 41,
   //    savings: {
   //       unit:'YEAR',
   //       value:20123,
   //       duration: 3
   //    },
   //    errors: []
   // }
   // const [result, setResult] = useState(loanScoreResult);

   const [result, setResult] = useState<LoanScoreResult>();



   // let env = 'prod';
   // if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development' || document.currentScript?.getAttribute('env') === 'qa') {
   //    env = 'qa';
   //    console.log('submit url is pointing at QA')
   // }

   //const url = `https://api.unohomeloans.com.au/application-api/${env}/accounts`;

   const onSubmit = async (request:LoanScoreRequest) => {

      setSubmitting(true);

      console.log('LoanScoreRequest', request)


      const baseUrl = props.baseUrl;
      const [err, axiosResult] = await to(axios.post(`${baseUrl}/loan-score`, request));
      let result:LoanScoreResult;
      if(err){
         result = { errors: [] }
         result.errors.push( { message: JSON.stringify(err), errorCode:'', parameter: ''});
      }else{
         result = axiosResult.data;
      }
      console.log('result', result)
      if(!result.errors.length){                  
         setResult(result);        
      } else {
         let errors = result.errors.map(x=>x.parameter + ' ' + x.message).join(',');                  
         setErrorMessage(errors);
      }
      setSubmitting(false);

   }


   return (
      <Container>
         <LoanScoreForm
         onSubmit={onSubmit}
         submitting={submitting}
         errorMessage={errorMessage}
         referrerLabel={props.referrerLabel}
         baseUrl={props.baseUrl}
         loanScoreResult={result}
         />

      </Container>
   );

}


export default App;
