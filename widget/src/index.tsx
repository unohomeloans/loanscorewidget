import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';


const root = document.getElementById('uno-loanScore-widget') as Element;

const referrerLabel = root.getAttribute('referrerLabel');
const baseUrl = root.getAttribute('baseUrl');
if(!referrerLabel)
    console.error('uno-loanScore-widget: referrerLabel is not defined!')
else if(!baseUrl)
    console.error('uno-loanScore-widget: baseUrl is not defined!')
else
    ReactDOM.render(<App referrerLabel={referrerLabel} baseUrl={baseUrl}/>, root);

console.log(`BaseUrl:${baseUrl}. NODE_ENV:${process.env.NODE_ENV} referrerLabel:${referrerLabel}`)