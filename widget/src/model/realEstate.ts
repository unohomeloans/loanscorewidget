export interface RealEstate {
    id?:number
    purpose:string
    estimatedValues:EstimatedValue[]
    address: Address
}

export interface EstimatedValue {
    basis:string
    value:number
}

export interface Address {
    postcode?:string
    suburb?:string
    state?:string
    fullAddress:string    
}