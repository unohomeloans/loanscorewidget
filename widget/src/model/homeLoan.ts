import { RealEstate } from "./realEstate";

export interface HomeLoan {
    id?:number
    loanAccounts:LoanAccount[]
    realEstates:RealEstate[]
}

export interface LoanAccount {
    remainingAmount: number
    remainingLoanTerm?: number
    remainingLoanTermDurationUnit?: string
    repaymentAmount: number
    repaymentFrequency: string
    repaymentType: string
    interestRate: number
    lenderCode: string
}