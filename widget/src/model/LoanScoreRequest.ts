import { User } from "./user";
import { RealEstate } from "./realEstate";
import { HomeLoan } from "./homeLoan";
import { Finances } from "./finances";
import { Savings } from "./loanScore";


export interface Employment {
    employed: boolean;
    employmentType: string;
    grossAnnualSalary: string;
}

export interface Applicant {
    emailAddress: string;
    firstName: string;
    lastName: string;
    firstHomeBuyer?: boolean;
    employments: Employment[];
}

export interface NewLoan {
    loanType: string;
    loanProductType?: string;
    repaymentType: string;
}

export interface ExistingLoan {
    loanAmount: string;
    remainingLoanTerm: string;
    remainingLoanTermMonths: string;
    lender: string;
    interestRate: string;
}

export interface Property {
    estimatedValue: string;
    existingLoan: ExistingLoan;
    purpose: string;
}

export interface LoanScoreRequest {
    newUser: User;
    finances: Finances;
    homeLoan: HomeLoan;
    property: RealEstate;
    referrerLabel: string;
}

export interface LoanScoreResult {
    loanScore?:number;
    savings?:Savings;
    loanScoreWebUrl?:string;
    errors:UnoApiError[]
}

export interface UnoApiError {
    errorCode:string
    message:string
    parameter:string
}