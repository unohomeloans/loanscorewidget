export interface LoanScorePreferences {
    loanFeature: LoanFeature;
    lender?: Lender;
}



export interface LoanFeature {
    repaymentType: string;
    offset?: boolean;
    redraw?: boolean;
    extraRepayment?: boolean;
}

export interface Lender {
    smallLenders: boolean;
    majorLenders: boolean;
}