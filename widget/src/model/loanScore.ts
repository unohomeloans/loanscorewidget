export interface LoanScore {
    homeLoanId: number;
    score: number;
    estimatedSavings: EstimatedSavings;
    uiUrls: UiUrl[];
}

export interface Savings {
    unit: string;
    duration: number;
    value: number;
}

export interface Lender {
    netSavings: Savings;
    grossSavings: Savings;
    totalOngoingFees: number;
    unoCashBack: number;
    lenderRebate: number;
    estimatedSwitchingCosts: number;
}

export interface EstimatedSavings {
    currentLender: Lender;
    newLender: Lender;
}

export interface UiUrl {
    rel: string;
    href: string;
}

