export interface Finances {
    householdIncome:HouseholdIncome    
}

export interface HouseholdIncome {
    grossAmount:number;
    frequency:string;
}