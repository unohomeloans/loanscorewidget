export interface LendersList {
    lenders:LenderItem[]
}

export interface LenderItem {
    code:string
    name:string
}