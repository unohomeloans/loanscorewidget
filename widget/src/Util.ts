const makeid = (length:number) : string => {
    let result           = '';
    const characters       = 'abcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

export const getDummyEmail = (referrer:string) : string => {
    let emailIndex = +(localStorage.getItem('emailIndex') || 0);
    emailIndex++;
    localStorage.setItem('emailIndex', emailIndex + '');
    let email = `user-${makeid(10)}@${referrer}.com.au`;
    return email;
}

