import { WebServer } from "./WebServer";
var fs = require('fs')
import path from 'path';
import { Config } from "./Config";
import { to } from "../widget/src/SharedHelpers";
import { setApiKey } from "./email-sender";
import { sendEmail } from "./email-sender";

console.log(`started, __dirname ${__dirname}`)
const filename = path.join(__dirname, 'config.json')
const text = fs.readFileSync(filename,'utf8');
let config:Config = JSON.parse(text); 

(async () => {
    
    setApiKey(config.sendgridApiKey)
    let app = new WebServer(config);
    app.init();
    
    // const [err, result] = await to(sendEmail('wallaceturner@gmail.com', 'New LoanScoreRequest','test body'));    
    // console.log('err', err)
    // console.log('result', result)
})();