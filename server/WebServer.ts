import express from 'express'
import { LoanScoreController } from './LoanScoreController';
import { LendersController } from './LendersController';
import { Config } from './Config';
var cors = require('cors');
var bodyParser = require('body-parser');

export class WebServer {
    

    constructor(private config:Config){
        
    }
    init(){
        const app: express.Application = express();
        app.use(cors({origin: '*'}));
        app.use(bodyParser.json());
        const port: number = 5001;

        
        app.use('/loan-score', new LoanScoreController(this.config).router);
        app.use('/lenders', new LendersController(this.config).router);
        

        app.listen(port, () => {
            console.log(`Listening on http://0.0.0.0:${port}/`);
        });

        
        
    }

    
}