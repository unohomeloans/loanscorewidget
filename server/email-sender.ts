const sgMail = require('@sendgrid/mail');

export function setApiKey(key:string){
    sgMail.setApiKey(key);
}

export function sendEmail(to:string, subject:string, body:string) : Promise<any> {
    const msg = {
        to: to,
        from: 'wallaceturner@gmail.com', 
        subject: subject,
        text: body,
        html: body,
      };
      
      return sgMail.send(msg);       
}


