import { Router, Request, Response } from 'express';
import { LoanScoreRequest } from '../widget/src/model/LoanScoreRequest'
import { Api } from './api';
import { Config } from './Config';
import { to } from '../widget/src/SharedHelpers';
import {sendEmail } from './email-sender';


export class LoanScoreController{
    router: Router = Router();    
    constructor(private config:Config){
        this.setupRoutes();
    }
    setupRoutes(){       

        this.router.post('/', async (req: Request, res: Response) => {
            
            let request:LoanScoreRequest = req.body as LoanScoreRequest;
            let referrer = this.config.referrerLabels.find(x=>x.name==request.referrerLabel)
            
            let apiError;
            let result;
            if(!referrer){
                apiError = "request missing referrerLabel"
            }else{
                const api = new Api(referrer, this.config.baseUrl);
                [ apiError, result ] = await api.getLoanScoreCombined(request);
            }

            res.send(result);

            if(apiError){
                for(let email of this.config.adminEmails){
                    const [err, emailSendResult] = await to(sendEmail(email, 'loanScore API call failed!', JSON.stringify(apiError)));
                    if(err){
                        console.log('email sending failed', err)
                    }
                }
                
            }else{
                let referrerLabel = this.config.referrerLabels.find(x=>x.name==request.referrerLabel)
            if(referrerLabel){
                for(let email of referrerLabel.emails){
                    let emailBody = `<pre>${JSON.stringify(request, null, 2)}</pre>`;
                    const [err, emailSendResult] = await to(sendEmail(email, 'New loanScore Request', emailBody));
                    if(err){
                        console.log('email sending failed', err)
                    }
                }
                
            }else{
                console.log('no referrerLabel for LoanScoreRequest: ' + JSON.stringify(request))
            }
            }
            
        });                
    }
}


