export interface Config {
    
    referrerLabels:ReferrerLabel[]
    sendgridApiKey:string
    baseUrl:string
    adminEmails:string[]
}

export interface ReferrerLabel {
    name:string
    emails:string[]
    referrer:string
    apiKey:string
}