import { Router, Request, Response } from 'express';
import { Api } from './api';
import { to } from '../widget/src/SharedHelpers';
import { Config } from './Config';


export class LendersController{
    router: Router = Router();    
    constructor(private config:Config){
        this.setupRoutes();
    }
    setupRoutes(){
        
        this.router.get('/', async (req: Request, res: Response) => {
            const referrerLabel = req.query?.referrerLabel;
            let referrer = this.config.referrerLabels.find(x=>x.name==referrerLabel)
            if(!referrer){
                res.send('referrerLabel missing');
            }else{
                const api = new Api(referrer, this.config.baseUrl);
                let [err, result ] = await to(api.getLenders());
                if(err){
                    res.send(err);
                }else{
                    res.send(result);
                }
            }
        });                
    }
}


