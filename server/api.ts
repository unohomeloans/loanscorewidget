import axios from 'axios';
import { User } from '../widget/src/model/user'
import { RealEstate } from '../widget/src/model/realEstate';
import { HomeLoan } from '../widget/src/model/homeLoan';
import { LoanScoreRequest, LoanScoreResult } from '../widget/src/model/LoanScoreRequest';
import { to } from '../widget/src/SharedHelpers';
import { LoanScore } from '../widget/src/model/loanScore';
import { LendersList, LenderItem } from '../widget/src/model/LendersList';
import { Finances } from '../widget/src/model/finances';
import { LoanScorePreferences } from '../widget/src/model/loanScorePreferences';
import { Config, ReferrerLabel } from './Config';



export class Api {
        
    config:any;
    constructor(private referrer:ReferrerLabel, private baseUrl:string){
      this.config = {
            headers: { 
                'referrer-code': referrer.referrer,
                'api-key': referrer.apiKey,
            }
        }
    }

    async postUser(user:User) :Promise<UnoApiResult> { 
        let result = await axios.post(this.baseUrl+'/users', user, this.config);
        
        return result.data;
    }

    async postRealEstate(realEstate:RealEstate, userId:number) :Promise<UnoApiResult> { 
        console.log('posting realEstate', realEstate)

        let result = await axios.post(this.baseUrl+ `/users/${userId}/real-estates`, realEstate, this.config);
        
        return result.data;
    }

    async postHomeLoan(homeLoan:HomeLoan, userId:number) :Promise<UnoApiResult> { 
        console.log('posting homeLoan', homeLoan)
        let result = await axios.post(this.baseUrl+ `/users/${userId}/home-loans`, homeLoan, this.config);
        
        return result.data;
    }

    async getLoanScore(homeLoanId:number, userId:number) :Promise<UnoApiResult> { 

        let result = await axios.get(this.baseUrl+ `/users/${userId}/home-loans/${homeLoanId}/loan-score`, this.config);
        
        return result.data;
    }

    async getLenders() :Promise<LenderItem[]> { 

        let result = await axios.get(this.baseUrl+ `/lenders`, this.config);
        
        return result.data;
    }

    async putFinances(finances:Finances, userId:number){
        console.log('posting finances', finances)
        let result = await axios.put(this.baseUrl+ `/users/${userId}/finances`, finances, this.config);
        
        return result.data;
    }

    async putLoanScore(preferences:LoanScorePreferences, userId:number, homeLoanId:number){
      console.log('posting preferences', preferences)
      let result = await axios.put(this.baseUrl+ `/users/${userId}/home-loans/${homeLoanId}/loan-score/preferences`, preferences, this.config);
      
      return result.data;
  }

    async getLoanScoreCombined(request:LoanScoreRequest) : Promise<[any, LoanScoreResult]> {
      
      let result:LoanScoreResult = { errors: [] };
      let userId:number = 0      
      let realEstate:RealEstate;
      let homeLoanId:number = 0

      {
        const [err, r1 ] = await to(this.postUser(request.newUser))
        if(err){
          console.error('error on postUser', err)
          this.populateErrors(err, result)          
          return [err, result];
        }
        
        let user = r1.data as User;
        userId = user.id!;        
      }      


      //finances
      {        
        const [err, r1 ] = await to(this.putFinances(request.finances, userId))
        if(err){
          console.error('error on putFinances', err)
          this.populateErrors(err, result)          
          return [err, result];
        }             
      }   
      
      {
        const [err, r2 ] = await to(this.postRealEstate(request.property, userId));
        if(err){
          console.error('error on postRealEstate', err)
          this.populateErrors(err, result)
          return [err, result];
        }
        realEstate = r2.data as RealEstate;
        console.log('r2', r2)
      }

      {
        request.homeLoan.realEstates.push(realEstate);
        const [err, r3 ] = await to(this.postHomeLoan(request.homeLoan, userId))
        if(err){
          console.error('error on postHomeLoan', err)
          this.populateErrors(err, result)
          return [err, result];
        }
        
        homeLoanId = (r3.data as HomeLoan).id!;
      }

      //putLoanScore
      {
        const preferences:LoanScorePreferences = { 
          loanFeature: {
            repaymentType: request.homeLoan.loanAccounts[0].repaymentType
          }
        }
        const [err, r3 ] = await to(this.putLoanScore(preferences, userId, homeLoanId))
        if(err){
          console.error('error on putLoanScore', err)
          this.populateErrors(err, result)
          return [err, result];
        }
        
      }

      {
        
        const [err, r3 ] = await to(this.getLoanScore(homeLoanId, userId))
        if(err){
          console.error('error on getLoanScore', err)
          this.populateErrors(err, result)
          return [err, result];
        }
        
        const loanScore = r3.data as LoanScore
        result.loanScore = loanScore.score;
        result.savings = loanScore.estimatedSavings.newLender.netSavings;
        result.loanScoreWebUrl = loanScore.uiUrls[0].href;
      }

      return [null, result];
    }

    populateErrors(err:any, result:LoanScoreResult){
        if(err.response?.data?.errors?.length){            
            result.errors.push(...err.response.data.errors)
        }
        else if(err.message){
          result.errors.push({ message:err.message, errorCode:'',  parameter: ''})
        }else{
          result.errors.push( { message: JSON.stringify(err), errorCode:'',  parameter: ''})
        }    
    }
}

export interface UnoApiResult {
    data:User | RealEstate | HomeLoan | LoanScore | LendersList;
}
