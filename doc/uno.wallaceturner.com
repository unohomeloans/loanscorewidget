server {


  root /var/www/uno.wallaceturner.com/public_html;
  server_name uno.wallaceturner.com;

  location / {

    try_files $uri $uri/ =404;
  }

  location ~ ^/(lenders|loan-score) {

    proxy_pass http://localhost:5001;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection keep-alive;
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }
}
