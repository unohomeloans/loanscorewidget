#create website
sudo mkdir -p /var/www/uno.wallaceturner.com/public_html
sudo chown -R www-data:www-data /var/www/uno.wallaceturner.com/public_html
sudo chmod -R 775 /var/www/uno.wallaceturner.com/public_html
sudo cp uno.wallaceturner.com /etc/nginx/sites-available/uno.wallaceturner.com
sudo ln -s /etc/nginx/sites-available/uno.wallaceturner.com /etc/nginx/sites-enabled/
 
 
sudo certbot --nginx -d uno.wallaceturner.com

#setup service
sudo cp uno.service /etc/systemd/system/
sudo systemctl enable uno.service
sudo systemctl start uno.service
sudo journalctl -fu uno.service