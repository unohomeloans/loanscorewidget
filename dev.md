# Development setup

## Widget
The project source consists of an ejected create-react-app. The reason for the ejection is to allow a single javascript file to be produced as the build output. See [here](https://stackoverflow.com/questions/59331493/combine-react-build-output-into-single-js-file) for more details.

After cloning the project may be starting by executing

    cd ./widget
    npm i
    npm start

The web app listens on port 8000 due to a CORS requirement and that the uno API (api.unohomeloans.com.au) has whitelisted `localhost:8000`

## Server

    cd ./server
    tsc
    node .\build\server\index.js

You can also run the server from within VS Code (F5) if debugging is required.

## Production build

To create a production build, use `npm run build`

This will produce a single javascript file located at `build\static\js\uno-loanScore-widget.min.js`
