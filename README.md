# UNO loanScore Server

The server will host a small node js application that acts as a proxy between the widget and the uno API.

Install nginx, nodejs and certbot

```
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
apt-get install -y nodejs nginx certbot python-certbot-nginx
```

Create nginx config
    
```
sudo vi /etc/nginx/sites-available/loanscore.yourdomain.com
```

Paste nginx config
```
server {
  root /var/www/loanscore.yourdomain.com/public_html;
  server_name loanscore.yourdomain.com;

  location / {
    try_files $uri $uri/ =404;
  }

  location ~ ^/(lenders|loan-score) {
    proxy_pass http://localhost:5001;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection keep-alive;
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }
}
```

Create website
```
sudo mkdir -p /var/www/loanscore.yourdomain.com/public_html
sudo chown -R www-data:www-data /var/www/loanscore.yourdomain.com/public_html
sudo chmod -R 775 /var/www/loanscore.yourdomain.com/public_html
sudo ln -s /etc/nginx/sites-available/loanscore.yourdomain.com /etc/nginx/sites-enabled/
sudo systemctl restart nginx
```

Setup HTTPS cert
```
sudo certbot --nginx -d loanscore.yourdomain.com
```

Setup loanscore.service

```
sudo vi /etc/systemd/system/loanscore.service
```

Insert into file: (replace user placeholder with desired user)
```
[Unit]
Description=loanscore
After=network.target

[Service]
Restart=always
RestartSec=10
KillSignal=SIGINT
WorkingDirectory=/opt/loanscore/
ExecStart=/usr/bin/node /opt/loanscore/build/server/index.js
User=<user>

[Install]
WantedBy=multi-user.target

```


Copy the provided `loanscore.zip` to the server

```
unzip -o loanscore.zip -d /opt/uno
cd /opt/uno
npm i
mv /opt/uno/uno-loanScore-widget.min.js /var/www/loanscore.yourdomain.com/public_html
```

Start service
```
sudo systemctl enable loanscore.service
sudo systemctl restart loanscore.service
```




# UNO loanScore Widget

This is a widget that can be dropped in to third party websites using the following 2 steps

*If using Wordpress then it is advised to use a 'Custom HTML' content Block.*

1: Drop the widget html hook into your page:

    <div id="uno-loanScore-widget" referrerLabel="ABC Loans" baseUrl="https://loanserver.nmb.com.au"></div>
    
2: Reference the loanScore widget javascript file:
    
    <script src="https://www.nmb.com.au/nmb/loanserver/uno-loanScore-widget.min.js"></script>


