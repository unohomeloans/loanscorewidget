Write-Host "starting build"
git reset --hard
git pull
git checkout -- .
git show --summary
git rev-parse --short HEAD | Out-File -FilePath .version -Encoding UTF8
$version = Get-Content .version

#build client
cd ./widget/
rmdir .\build\ -r -fo
npm i
npm run build


#build service
cd ../server
npm i
tsc

Write-Host "build completed successfully" 
cd ..

$zipFile = "loanscore.zip"
Write-Host "Creating installation zip $zipFile"
del $zipFile
7z a $zipFile .version
7z a $zipFile ./widget/build/static/js/*.js
7z a $zipFile ./server/package.json
7z a $zipFile ./server/build/

git show --summary


#scp loanscore.zip wal@wallaceturner.com:/var/www/html/